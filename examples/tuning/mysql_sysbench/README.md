1. Prepare the environment
```bash
sh prepare.sh
```
2. Start to tuning
```bash
atune-adm tuning --project mysql_sysbench --detail mysql_sysbench_client.yaml
```
3. Restore the environment
```bash
atune-adm tuning --restore --project mysql_sysbench
```
*Note*
If running prepare.sh failed, you may need to install MySQL and sysbench manually according to the guide:
MySQL - [https://blog.csdn.net/weixin_43214408/article/details/116895091](https://blog.csdn.net/weixin_43214408/article/details/116895091)
sysbench - [https://blog.csdn.net/weixin_43214408/article/details/116898751](https://blog.csdn.net/weixin_43214408/article/details/116898751)
